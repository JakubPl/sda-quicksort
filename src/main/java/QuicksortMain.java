import java.util.Arrays;

public class QuicksortMain {
    public static void main(String[] args) {
        final int[] ints = {1, 5, 3, 3, 2};
        quicksort(ints, 0, 4);
        System.out.println(Arrays.toString(ints));
    }

    public static void quicksort(int[] tab, int l, int r) {
        if(l < r) {
            int lastChangedPosition = divideArrays(tab, l, r);
            //left sort
            quicksort(tab, l, lastChangedPosition - 1);
            quicksort(tab, lastChangedPosition + 1, r);
        }
    }

    private static int divideArrays(int[] tab, int l, int r) {
        final int middleIndex = (l + r) / 2;
        final int middleValue = tab[middleIndex];
        swap(tab, middleIndex, r);

        int lastChangedPosition = l;
        for(int i = l; i < r; i ++) {
            if(tab[i] < middleValue) {
                swap(tab, i, lastChangedPosition);
                lastChangedPosition ++;
            }
        }

        swap(tab, lastChangedPosition, r);
        return lastChangedPosition;

    }

    private static void swap(int[] tab, int l, int r) {
        int tmp = tab[l];
        tab[l] = tab[r];
        tab[r] = tmp;
    }
}
